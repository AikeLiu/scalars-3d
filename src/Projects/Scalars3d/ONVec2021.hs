{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Scalars3d.ONVec2021 where

import qualified Blocks.ScalarBlocks                as SB
import           Bootstrap.Bounds.Spectrum          (setGaps, setTwistGap,
                                                     unitarySpectrum)
import           Bootstrap.Math.Linear              (fromRows, fromV, toV)
import           Bounds.Scalars3d.ONRep             (ONRep (..))
import qualified Bounds.Scalars3d.ONVec             as ONVec
import           Control.Monad                      (forM_, void, when)
import           Control.Monad.IO.Class             (liftIO)
import           Control.Monad.Reader               (asks, local)
import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.List.Split                    (chunksOf)
import qualified Data.Map.Strict                    as Map
import           Data.Maybe                         (fromMaybe, listToMaybe)
import           Data.Text                          (Text, pack, splitOn, strip,
                                                     unpack)
import qualified Data.Text
import           Data.Time.Clock                    (NominalDiffTime)
import           Hyperion
import           Bootstrap.Math.AffineTransform (AffineTransform (..))
import           Hyperion.Bootstrap.Bound           (Bound (..),
                                                     BoundFiles (..))
import qualified Hyperion.Bootstrap.Bound           as Bound
import           Hyperion.Bootstrap.DelaunaySearch  (DelaunayConfig (..),
                                                     delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main            (unknownProgram)
import           Hyperion.Bootstrap.OPESearch       (BilinearForms (..),
                                                     OPESearchConfig (..),
                                                     TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch       as OPE
import qualified Hyperion.Bootstrap.Params          as Params
import qualified Hyperion.Bootstrap.SDPDeriv        as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.BFGS   as BFGS
import qualified Hyperion.Bootstrap.SDPDeriv.Newton as Newton
import qualified Hyperion.Database                  as DB
import qualified Hyperion.Log                       as Log
import qualified Hyperion.Slurm                     as Slurm
import           Hyperion.Util                      (hour, minute)
import           Linear.V                           (V)
import           Numeric                            (readFloat, readSigned)
import           Numeric.Rounded                    (Rounded, RoundingMode (..))
import           Projects.Scalars3d.Defaults        (defaultBoundConfig,
                                                     defaultDelaunayConfig)
import           Projects.Scalars3d.ONVec2021Affine
import           SDPB                               (Params (..))
import qualified SDPB
import           System.Directory                   (removePathForcibly)
-- for arg parsing (need to rewrite this?):
import           Data.Typeable                      (Typeable)
import           Text.Regex.TDFA                    ((=~))

remoteONVecOPESearch
 :: TrackedMap Cluster (Bound Int ONVec.ONVec) FilePath
 -> TrackedMap Cluster (Bound Int ONVec.ONVec) (V 2 Rational)
 -> Rational
 -> Rational
 -> Bound Int ONVec.ONVec
 -> Cluster Bool
remoteONVecOPESearch checkpointMap lambdaMap thetaMin thetaMax =
  OPE.remoteOPESearch (cPtr (static onVecOPESearchCfg)) checkpointMap lambdaMap initialBilinearForms
 where
   -- TODO: make sure this is correct
   initialBilinearForms =
     BilinearForms 1e-16 [(Nothing, OPE.thetaIntervalFormApprox 1e-16 thetaMin thetaMax)]
   onVecOPESearchCfg =
     OPESearchConfig setLambda ONVec.getExternalMat OPE.queryAllowedBoolFunction
   setLambda lambda bound = bound
                               { boundKey = (boundKey bound)
                                 { ONVec.objective = ONVec.Feasibility (Just lambda) }
                               }

-- remoteComputeONVecBound :: Bound Int ONVec.ONVec -> Cluster SDPB.Output
-- remoteComputeONVecBound bound = do
--   workDir <- newWorkDir bound
--   jobTime <- asks (Slurm.time . clusterJobOptions)
--   result <-
--     continueSDPBCheckpointed (workDir, solverParams bound) $
--     remoteEvalJob (static (remoteFnJob compute)) (jobTime, bound, workDir)
--   Log.info "Computed" (bound, result)
--   DB.insert (DB.KeyValMap "computations") bound result
--   return result
--   where
--     compute (jobTime', bound', workDir') = do
--       -- We ask SDPB to terminate within 90% of the jobTime. This
--       -- should ensure sufficiently prompt exit as long as an
--       -- iteration doesn't take 10% or more of the jobTime.
--       solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime') (solverParams bound')
--       computeClean bound' { solverParams = solverParams' } workDir'

remoteComputeONVecBoundMulti :: [Bound Int ONVec.ONVec] -> Cluster ()
remoteComputeONVecBoundMulti bounds = do
  workDir <- newWorkDir (head bounds)
  jobTime <- asks (Slurm.time . clusterJobOptions)
  remoteEvalJob $
    static compute `ptrAp` cPure jobTime `cAp` cPure bounds `cAp` cPure workDir
  where
    computationsMap :: DB.KeyValMap (Bound Int ONVec.ONVec) SDPB.Output
    computationsMap = DB.KeyValMap "computations"
    compute jobTime bounds' workDir' = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime) (solverParams (head bounds'))
      forM_ (zip bounds' [1 :: Int ..]) $ \(bound,i) ->
        DB.lookup computationsMap bound >>= \case
        Just _  -> pure ()
        Nothing -> do
          Log.text $ "Computing bound " <> Log.showText i <> " of " <> Log.showText (length bounds') <> "."
          let boundFiles = Bound.defaultBoundFiles workDir'
          result <- Bound.computeClean' (bound { solverParams = solverParams' }) boundFiles
          when (SDPB.isFinished result) $
            DB.insert computationsMap bound result
          liftIO $ mapM_ removePathForcibly [checkpointDir boundFiles, outDir boundFiles]

onVecFeasibleDefaultGaps
  :: V 2 Rational
  -> Maybe (V 2 Rational)
  -> Rational
  -> Int
  -> ONVec.ONVec
onVecFeasibleDefaultGaps deltaExts mLambda ngroup nmax = ONVec.ONVec
  { externalDims = ONVec.ExternalDims {..}
  , nGroup       = ngroup
   -- (ChannelType l ONRep, Rational gap)
  , spectrum     = setGaps [ (singChannel, 3)
                           , (vecChannel, 3)
                           ] $ setTwistGap 1e-6 $ unitarySpectrum
  , objective    = ONVec.Feasibility mLambda
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    singChannel = ONVec.ChannelType 0 ONSinglet
    vecChannel  = ONVec.ChannelType 0 ONVector
    (deltaPhi,deltaS) = fromV deltaExts

onVecGFFNavigatorDefaultGaps
  :: V 2 Rational
  -> Maybe (V 2 Rational)
  -> Rational
  -> Int
  -> ONVec.ONVec
onVecGFFNavigatorDefaultGaps deltaExts mLambda ngroup nmax = ONVec.ONVec
  { externalDims = ONVec.ExternalDims {..}
  , nGroup       = ngroup
  , objective    = ONVec.GFFNavigator mLambda
  , spectrum     = setGaps [ (singChannel, 3)
                           , (vecChannel, 3)
                           ] unitarySpectrum
  , blockParams  = Params.blockParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  }
  where
    singChannel = ONVec.ChannelType 0 ONSinglet
    vecChannel  = ONVec.ChannelType 0 ONVector
    (deltaPhi, deltaS) = fromV deltaExts

-- OPE SEARCH SUPPORT CODE
onVecDimVector :: Bound prec ONVec.ONVec -> V 2 Rational
onVecDimVector Bound { boundKey = i } =
  toV (ONVec.deltaPhi (ONVec.externalDims i), ONVec.deltaS (ONVec.externalDims i))

newONVecCheckpointMap
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int ONVec.ONVec) FilePath)
newONVecCheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine onVecDimVector mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "onVecCheckpoints")

newONVecLambdaMap
  :: (FromJSON l, ToJSON l, Typeable l)
  => AffineTransform 2 Rational
  -> Maybe l
  -> Cluster (TrackedMap Cluster (Bound Int ONVec.ONVec) l)
newONVecLambdaMap affine mLambda =
 liftIO (OPE.affineLocalityMap affine onVecDimVector mLambda) >>=
 OPE.mkPersistent (DB.KeyValMap "onVecLambdas")

--- DELAUNAY SEARCH STARTS HERE
deltaExtToV :: Bound Int ONVec.ONVec -> V 2 Rational
deltaExtToV Bound { boundKey = ONVec.ONVec { externalDims = eds } } =
  toV (ONVec.deltaPhi eds, ONVec.deltaS eds)

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

data ONVecDelaunaySearchData = ONVecDelaunaySearchData
  { nmax              :: Int
  , affine            :: AffineTransform 2 Rational
  , initialCheckpoint :: Maybe FilePath
  , initialDisallowed :: [V 2 Rational]
  , initialAllowed    :: [V 2 Rational]
  , solverPrecision   :: Int
  , delaunayConfig    :: DelaunayConfig
  , jobTime           :: NominalDiffTime
  , jobType           :: MPIJob
  , jobMemory         :: Text
  }

onVecDelaunaySearch :: Rational -> ONVecDelaunaySearchData -> Cluster ()
onVecDelaunaySearch nGroup ONVecDelaunaySearchData{..} =
  local (setJobType jobType . setJobTime jobTime . setJobMemory jobMemory) $ void $ do
  delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
    bound deltaExts = Bound
      { boundKey = onVecFeasibleDefaultGaps deltaExts Nothing nGroup nmax
      , precision = solverPrecision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = solverPrecision }
      , boundConfig = defaultBoundConfig
      }
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]

-- | START OF BOUNDS PROGRAMS --------------------------------------------------
boundsProgram :: Text -> Cluster ()

boundsProgram "ONVecAllowed_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (25*minute) . setJobMemory "90G") $ void $
  Bound.remoteCompute $
--bound (toV (0.51909, 1.51136)) -- should be ALLOWED    (primal feasible jump)
  bound (toV (0.50909, 1.61136)) -- should be DISALLOWED (dual feasible jump)
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = (onVecFeasibleDefaultGaps deltaExts Nothing ngroup nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

-- bounds program for first delaunay search
boundsProgram "ONVec_delaunay_ngroup2_nmax6" =
  onVecDelaunaySearch ngroup searchData
  where
    ngroup = 2
    searchData = ONVecDelaunaySearchData
        { nmax = 6
        , affine = onVecAffineNgroup2Nmax6
        , initialCheckpoint = Nothing
        , initialDisallowed = [toV (0.51909, 1.51136)]
        , initialAllowed = [toV (0.50909, 1.61136)]
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 3*hour -- was 8 hr
        , jobType = MPIJob 1 16
        , jobMemory = "90G"
        }

boundsProgram "ONVecAllowed_test_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (1*hour) . setJobMemory "90G") $ void $
  Bound.remoteCompute $
  bound (toV (0.5192, 1.510)) -- should be ALLOWED    (primal feasible jump)
--bound (toV (0.5185, 1.525)) -- should be DISALLOWED (dual feasible jump)
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = (onVecFeasibleDefaultGaps deltaExts Nothing ngroup nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "ONVec_delaunay_ngroup2_nmax10" =
  onVecDelaunaySearch ngroup searchData
  where
    ngroup = 2
    searchData = ONVecDelaunaySearchData
        { nmax = 10
        , affine = onVecAffineNgroup2Nmax10
        , initialCheckpoint = Nothing
        , initialDisallowed = [toV (0.5185, 1.525)] -- should be more pts?
        , initialAllowed = [toV (0.5192, 1.510)]
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 10 200
        , jobTime = 8*hour -- was 8 hr
        , jobType = MPIJob 1 28
        , jobMemory = "90G"
        }

boundsProgram "ONVec_delaunay_ngroup3_nmax6" =
  onVecDelaunaySearch ngroup searchData
  where
    ngroup = 3
    searchData = ONVecDelaunaySearchData
        { nmax = 6
        , affine = onVecAffineGen 3 6
        , initialCheckpoint = Nothing
        , initialDisallowed = [toV (0.519, 1.16)]
        , initialAllowed = [toV (0.5193, 1.595)]
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 2*hour -- was 8 hr
        , jobType = MPIJob 1 16
        , jobMemory = "90G"
        }

boundsProgram "ONVec_delaunay_ngroup3_nmax10" =
  onVecDelaunaySearch ngroup searchData
  where
    ngroup = 3
    searchData = ONVecDelaunaySearchData
        { nmax = 10
        , affine = onVecAffineGen 3 6
        , initialCheckpoint = Nothing
        , initialDisallowed = [toV (0.519, 1.16)]
        , initialAllowed = [toV (0.5193, 1.595)]
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 10 200
        , jobTime = 4*hour -- was 8 hr
        , jobType = MPIJob 1 28
        , jobMemory = "90G"
        }

boundsProgram "ONVec_allowedOPESearch_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobMemory "16G") $ do
  checkpointMap <- newONVecCheckpointMap affine Nothing
  lambdaMap     <- newONVecLambdaMap affine Nothing
  mapConcurrently_ (remoteONVecOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
    [ (toV (0.51909, 1.51136), OPE.thetaVectorApprox 1e-16 0.878) --arctan(1.205)
    , (toV (0.50909, 1.61136), OPE.thetaVectorApprox 1e-16 1.0)
    ]
  where
    ngroup = 2
    nmax = 6
    affine = onVecAffineGen 2 6
    bound (deltaExts, lambda) = Bound
      { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      -- | It is very important to use Params.jumpFindingParams when doing an OPESearch
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "ONVec_allowedOPESearch_ngroup10_nmax6" =
  local (setJobType (MPIJob 1 4) . setJobMemory "16G") $ do
  checkpointMap <- newONVecCheckpointMap affine Nothing
  lambdaMap     <- newONVecLambdaMap affine Nothing
  mapConcurrently_ (remoteONVecOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
    [ (toV (0.513, 1.85), OPE.thetaVectorApprox 1e-16 0.878) --arctan(1.205)
    , (toV (0.50909, 1.61136), OPE.thetaVectorApprox 1e-16 1.0)
    ]
  where
    ngroup = 2
    nmax = 6
    affine = onVecAffineGen 2 6
    bound (deltaExts, lambda) = Bound
      { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      -- | It is very important to use Params.jumpFindingParams when doing an OPESearch
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "ONVec_islandOPESearch_old_ngroup2_nmax10" =
   local (setJobType (MPIJob 1 16) . setJobTime (1*hour) . setJobMemory "80G") $ do
   checkpointMap <- newONVecCheckpointMap affine Nothing
   lambdaMap     <- newONVecLambdaMap affine Nothing
   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
     (remoteONVecOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
   where
     nmax = 10
     ngroup = 2
     affine = onVecAffineGen 2 10
     bound deltaExts = Bound
       { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambdaInit) ngroup nmax
       , precision = 768
       , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
       , boundConfig = defaultBoundConfig
       }
     delaunayConfig = defaultDelaunayConfig 10 180
     lambdaInit = OPE.thetaVectorApprox 1e-16 0.878
     initialPts = Map.fromList $
       [(p, Just True)  | p <- initialAllowed] ++
       [(p, Just False) | p <- initialDisallowed]
       where
         initialDisallowed = [toV (0.5185, 1.525)]
         initialAllowed = [toV (0.51909, 1.51136)]

boundsProgram "ONVec_GFFNavigatorNoLambda_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 16) . setJobTime (6*hour) . setJobMemory "80G") $
  mapConcurrently_ (remoteComputeONVecBoundMulti . map bound) $ chunksOf 16 plotPts
  where
    ngroup = 2
    nmax = 6
    plotPts = (,) <$> [0.51,  0.511 .. 0.53]  <*> [1.4,  1.41 .. 1.6]
    -- plotPts = (,) <$> [0.520,  0.521 .. 0.525]  <*> [1.55,  1.56 .. 1.6]
    bound (deltaPhi, deltaS) = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) Nothing ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "ONVec_GFFNavigatorYesLambda_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 20) . setJobTime (8*hour) . setJobMemory "90G") $
  mapConcurrently_ (remoteComputeONVecBoundMulti . map bound) $ chunksOf 10 plotPts
  where
    ngroup = 2
    nmax = 6
    plotPts = (,) <$> [0.51,  0.511 .. 0.53]  <*> [1.4,  1.41 .. 1.6]
    -- plotPts = (,) <$> [0.520,  0.521 .. 0.525]  <*> [1.55,  1.56 .. 1.6]
    bound (deltaPhi, deltaS) = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) (Just lambdaInit) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 640 }
      , boundConfig = defaultBoundConfig
      }
    lambdaInit = OPE.thetaVectorApprox 1e-16 0.878

boundsProgram "ONVec_GFFNavigatorNoLambda_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 24) . setJobTime (10*hour) . setJobMemory "90G") $
  mapConcurrently_ (remoteComputeONVecBoundMulti . map bound) $ chunksOf 10 plotPts
  where
    ngroup = 2
    nmax = 10
    plotPts = (,) <$> [0.518,  0.5182 .. 0.5210]  <*> [1.50,  1.502 .. 1.53]
    bound (deltaPhi, deltaS) = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) Nothing ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "ONVec_GFFNavigatorBFGS_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "90G") $
  do
    result :: (V 2 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig
    Log.info "Finished BFGS search" result
  where
    bbMin = toV (0.50, 1.3)
    bbMax = toV (0.54, 1.7)
    bfgsConfig = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.515, 1.56)
      , checkpointMapName = Nothing
      }
    mkBound :: V 2 Rational -> Bound Int ONVec.ONVec
    mkBound v = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 6
        lambdaInit = OPE.thetaVectorApprox 1e-16 0.878

boundsProgram "ONVec_GFFNavigatorBFGS_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "90G") $ do
    result :: ([V 2 Rational], BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
      SDPDeriv.remoteBFGSExtremize islandExtConfig bjConfig extremaDirs
    Log.info "Finished BFGS search" result
--    result :: (V 2 Rational, [BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
--      SDPDeriv.remoteBFGSBound bfgsConfig bjConfig "onVecCheckpoints"
--    Log.info "Finished BFGS search" result
  where
    extremaDirs = [toV (1,0), toV (-1,0), toV (0,1), toV (0,-1)]
    bbMin = toV (0.50, 1.3)
    bbMax = toV (0.54, 1.7)
    bfgsConfig' = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Nothing
      }
    islandExtConfig = BFGS.defaultExtremaConfig bfgsConfig'
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.515, 1.56)
      , checkpointMapName = Nothing
      }
    mkBound :: V 2 Rational -> Bound Int ONVec.ONVec
    mkBound v = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps v (Just lambdaInit) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 2
        nmax = 10
        lambdaInit = OPE.thetaVectorApprox 1e-16 0.878

boundsProgram "ONVec_GFFNavigatorBFGS_ngroup20_nmax12" = --0.5065,1.943,
  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "90G") $ do
  result :: ([V 3 Rational], BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
    SDPDeriv.remoteBFGSExtremize islandExtConfig bjConfig extremaDirs
  Log.info "Finished BFGS search" (fst result)
  where
    extremaDirs = [toV (0,0,1), toV (0,0,-1)]
    bbMin = toV (0.5062, 1.93, -0.8)
    bbMax = toV (0.5068, 1.96,  0.8)
    bfgsConfig' = BFGS.MkConfig
      { BFGS.epsGradHess         = 1e-9
      , BFGS.stepResolution      = 1e-32
      , BFGS.gradNormThreshold   = 1e-7
      , BFGS.stopOnNegative      = True
      , BFGS.boundingBoxMin      = bbMin
      , BFGS.boundingBoxMax      = bbMax
      , BFGS.initialHessianGuess = Just $ fromRows $ toV
                                   ( toV (6544350,  -187921,  3634.85)
                                   , toV (-187921,  5824.31, -112.019)
                                   , toV (3634.85, -112.019,  2.82341)
                                   )
      }
    islandExtConfig = BFGS.MkIslandExtConfig
      { bfgsConfig         = bfgsConfig'
      , goalToleranceObj   = 1e-4
      , goalToleranceGrad  = 1e-1
      , lineSearchDecrease = 0.8
      , maxIters           = Nothing
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5064, 1.955, 0.1)
      , checkpointMapName = Nothing
      }
    mkBound :: V 3 Rational -> Bound Int ONVec.ONVec
    mkBound v = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 20
        nmax = 12
        (deltaPhi, deltaS, theta) = fromV v
        lambda = OPE.thetaVectorApprox 1e-16 theta

boundsProgram "ONVec_GFFNavigatorEvaltest_ngroup20_nmax12" = --0.5065,1.943,
  local (setJobType (MPIJob 1 28) . setJobTime (3*hour) . setJobMemory "90G") $ do
  result :: ([BFGS.BFGSData n (Rounded 'TowardZero 200)]) <-
    SDPDeriv.remoteEvalGrads bjConfig gradPoints
  Log.info "Finished BFGS search" result
  where
    gradPoints = fmap (\dv -> toV dv + toV (0.5065, 1.943, 0.2))
                 [ ( 1e-6, 0,    0   )
                 , (-1e-6, 0,    0   )
                 , (0,     1e-6, 0   )
                 , (0,    -1e-6, 0   )
                 , (0,    0,     1e-6)
                 , (0,    0,    -1e-6)
                 ]
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (0.5064, 1.955, 0.1)
      , checkpointMapName = Nothing
      }
    mkBound :: V 3 Rational -> Bound Int ONVec.ONVec
    mkBound v = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup = 20
        nmax = 12
        (deltaPhi, deltaS, theta) = fromV v
        lambda = OPE.thetaVectorApprox 1e-16 theta


boundsProgram p = case parseCommand p of
  Just spec -> boundsProgramGen (programName spec) spec
  Nothing   -> unknownProgram p

-- | I'm tired of recompiling, so let's parse some args

data ProgramSpec = ProgramSpec
  { rawCommand  :: Text
  , programName :: Text
  , ngroup      :: Rational
  , nmax        :: Int
  , argMap      :: Map.Map Text Text
  } deriving Show

boundsProgramGen :: Text -> ProgramSpec -> Cluster ()
boundsProgramGen "allowed" ProgramSpec{..} =
  local (setJobType (MPIJob 1 28) . setJobTime (1*hour) . setJobMemory "90G") $
  void $ Bound.remoteCompute $
  bound (toV point)
  where
    bound deltaExts = Bound
      { boundKey = (onVecFeasibleDefaultGaps deltaExts lambdaMaybe ngroup nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
      , boundConfig = defaultBoundConfig
      }
    point = fromMaybe (0.5192, 1.510) $ do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    lambdaMaybe = OPE.thetaVectorApprox 1e-16 <$> do
      thetaText <- Map.lookup "theta" argMap
      parseRational thetaText
    prec = fromMaybe 768 $ do
      precText <- Map.lookup "precision" argMap
      return (read (unpack precText) :: Int)

boundsProgramGen "allowedOPESearch" ProgramSpec{..} =
  local (setJobType (MPIJob 1 24) . setJobTime (1*hour) . setJobMemory "90G") $ void $
  do
    checkpointMap <- newONVecCheckpointMap affine Nothing
    lambdaMap     <- newONVecLambdaMap affine Nothing
    remoteONVecOPESearch checkpointMap lambdaMap (-0.05) 1.5 $
      bound (toV point, OPE.thetaVectorApprox 1e-16 theta)
  where
    bound (deltaExts, lambda) = Bound
      { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      -- | It is very important to use Params.jumpFindingParams when doing an OPESearch
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
      , boundConfig = defaultBoundConfig
      }
    affine = onVecAffineGen ngroup nmax
    point = fromMaybe (0.5192, 1.510) $ do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    theta = fromMaybe 1.0 $ do
      thetaText <- Map.lookup "theta" argMap
      parseRational thetaText
    prec = fromMaybe 768 $ do
      precText <- Map.lookup "precision" argMap
      return (read (unpack precText) :: Int)

boundsProgramGen "gridOPESearch" ProgramSpec{..} =
  local (setJobType (MPIJob 1 12) . setJobTime (2*hour) . setJobMemory "40G") $ void $
  do
    checkpointMap <- newONVecCheckpointMap affine Nothing
    lambdaMap     <- newONVecLambdaMap affine Nothing
    mapConcurrently_ (remoteONVecOPESearch checkpointMap lambdaMap 0.05 1.5 . bound) $
      [(toV pt, OPE.thetaVectorApprox 1e-16 theta) | pt <- allPoints]
  where
    bound (deltaExts, lambda) = Bound
      { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambda) ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      -- | It is very important to use Params.jumpFindingParams when doing an OPESearch
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
      , boundConfig = defaultBoundConfig
      }
    allPoints = do
      nx <- [-2, -1 .. 2]
      ny <- [-2, -1 .. 2]
      [(nx*dx + fst point, ny*dy + snd point)]
    affine = onVecAffineGen ngroup nmax
    point = fromMaybe (0.5192, 1.510) $ do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    theta = fromMaybe 1.0 $ do
      thetaText <- Map.lookup "theta" argMap
      parseRational thetaText
    prec = fromMaybe 768 $ do
      precText <- Map.lookup "precision" argMap
      return (read (unpack precText) :: Int)
    dx = fromMaybe 0.001 $ do
      dxText <- Map.lookup "dx" argMap
      parseRational dxText
    dy = fromMaybe 0.001 $ do
      dxText <- Map.lookup "dy" argMap
      parseRational dxText

boundsProgramGen "islandOPESearch" ProgramSpec{..} =
   local (setJobType (MPIJob 1 24) . setJobTime (5*hour) . setJobMemory "90G") $ do
   checkpointMap <- newONVecCheckpointMap affine Nothing
   lambdaMap     <- newONVecLambdaMap affine Nothing
   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
     (remoteONVecOPESearch checkpointMap lambdaMap 0.05 1.5 . bound)
   where
     affine = onVecAffineGen ngroup nmax
     bound deltaExts = Bound
       { boundKey = onVecFeasibleDefaultGaps deltaExts (Just lambdaInit) ngroup nmax
       , precision = 768
       , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
       , boundConfig = defaultBoundConfig
       }
     delaunayConfig = defaultDelaunayConfig 15 180
     lambdaInit = OPE.thetaVectorApprox 1e-16 theta
     initialPts = Map.fromList $
       [(p, Just True)  | p <- initialAllowed] ++
       [(p, Just False) | p <- initialDisallowed]
       where
         initialDisallowed = []
         initialAllowed = [point]
     point = fromMaybe (affineShift affine) (fmap toV userPoint)
     theta = fromMaybe 0.2 userTheta
     userPoint = do
       pointText <- Map.lookup "point" argMap
       parseTuple2 pointText
     userTheta = do
       thetaText <- Map.lookup "theta" argMap
       parseRational thetaText

boundsProgramGen "delaunay" ProgramSpec{..} =
  onVecDelaunaySearch ngroup searchData
  where
    searchData = ONVecDelaunaySearchData
        { nmax = nmax
        , affine = onVecAffineNgroup2Nmax6
        , initialCheckpoint = Nothing
        , initialDisallowed = [toV (0.51909, 1.51136)]
        , initialAllowed = [toV (0.50909, 1.61136)]
        , solverPrecision = 768
        , delaunayConfig = defaultDelaunayConfig 15 300
        , jobTime = 4*hour -- was 8 hr
        , jobType = MPIJob 1 16
        , jobMemory = "90G"
        }

boundsProgramGen "GFFNavigatorPoint" ProgramSpec{..} =
  local (setJobType (MPIJob 1 24) . setJobTime (4*hour) . setJobMemory "90G") $
  void $ Bound.remoteCompute $ bound point
  where
    point = fromMaybe (0.522, 1.581) userPoint
    userPoint = do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    userLambda = fmap (OPE.thetaVectorApprox 1e-16) $ do
       thetaText <- Map.lookup "theta" argMap
       parseRational thetaText
    bound (deltaPhi, deltaS) = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) userLambda ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 640 }
      , boundConfig = defaultBoundConfig
      }

boundsProgramGen "GFFNavigatorBox" ProgramSpec{..} =
  local (setJobType (MPIJob 1 24) . setJobTime (3*hour) . setJobMemory "90G") $
  mapConcurrently_ (remoteComputeONVecBoundMulti . map bound) $ chunksOf 1 allPoints
  where
    -- plotPts = (,) <$> [0.520,  0.521 .. 0.525]  <*> [1.55,  1.56 .. 1.6]
    bound (deltaPhi, deltaS) = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps (toV (deltaPhi, deltaS)) userLambda ngroup nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = prec }
      , boundConfig = defaultBoundConfig
      }
    allPoints = do
      nx <- [-2,-1,0,1,2]
      ny <- [-2,-1,0,1,2]
      [(nx*dx + fst point, ny*dy + snd point)]
    point = fromMaybe (0.5192, 1.510) $ do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    userLambda = fmap (OPE.thetaVectorApprox 1e-16) $ do
       thetaText <- Map.lookup "theta" argMap
       parseRational thetaText
    dx = fromMaybe 0.001 $ do
      dxText <- Map.lookup "dx" argMap
      parseRational dxText
    dy = fromMaybe 0.001 $ do
      dxText <- Map.lookup "dy" argMap
      parseRational dxText
    prec = fromMaybe 640 $ do
      precText <- Map.lookup "precision" argMap
      return (read (unpack precText) :: Int)

boundsProgramGen "GFFNavigatorNewton3" ProgramSpec{..}=
  local (setJobType (MPIJob 1 24) . setJobTime (8*hour) . setJobMemory "90G") $ do
  result :: (V 3 Rational, [SDPDeriv.Jet2 n (Rounded 'TowardZero 200)]) <-
    SDPDeriv.remoteNewtonBound nsConfig bjConfig
  Log.info "Finished Newton search" result
  where
    nsConfig = Newton.MkConfig
      { epsGradHess         = 1e-9
      , stepResolution      = 1e-32
      , stepNormThreshold   = 1e-9
      }
    bjConfig = SDPDeriv.GetBoundJetConfig
      { centeringIterations = 10
      , fileTreatment = Bound.keepOutAndCheckpoint
      , boundClosure = cPtr (static mkBound)
      , valFromObjClosure = cPtr (static (SDPDeriv.MkFractionalMap (\y -> y/(1-y))))
      , initialPoint = toV (deltaPhi0, deltaS0, theta0)
      , checkpointMapName = Nothing
      }
    (deltaPhi0, deltaS0) = fromMaybe (0.515, 1.56) $ do
      pointText <- Map.lookup "point" argMap
      parseTuple2 pointText
    theta0 = fromMaybe 0.878 $ do
       thetaText <- Map.lookup "theta" argMap
       parseRational thetaText

    mkBound :: V 3 Rational -> Bound Int ONVec.ONVec
    mkBound v = Bound
      { boundKey = onVecGFFNavigatorDefaultGaps v2 (Just lambda) ngroup' nmax'
      , precision = (Params.blockParamsNmax nmax').precision
      , solverParams = (Params.optimizationParams nmax')
                       { precision = 768
                       , dualityGapThreshold = 1e-30
                       }
      , boundConfig = defaultBoundConfig
      }
      where -- this is so static stuff works correctly
        ngroup' = 2
        nmax' = 10
        (deltaPhi, deltaS, theta) = fromV v
        v2 = toV (deltaPhi, deltaS)
        lambda = OPE.thetaVectorApprox 1e-16 theta

boundsProgramGen progname _ = unknownProgram progname

-- | END OF BOUNDS PROGRAMS ----------------------------------------------------

-- | string plumbing for parsing commands and generating programs on the fly
-- don't read this, it's a mess
-- general command format: something like ONVec_delaunay_ngroup2_nmax6_a=b;c=d;e
parseCommand :: Text -> Maybe ProgramSpec
parseCommand p
  | s =~ parsePattern = do
      ngroup <- parseRational $ pack (params !! 1)
      return ProgramSpec { rawCommand = p
                         , programName = pack (params !! 0)
                         , ngroup = ngroup
                         , nmax = read (params !! 2) :: Int
                         , argMap = kvExtract $ pack (params !! 3)
                         }
  | otherwise = Nothing
  where
    s = unpack p
    parsePattern = "^ONVec_([a-zA-Z0-9]*)_ngroup([0-9]*)_nmax([0-9]*)_(.*)$" :: String
    (_,_,_,params) = s =~ parsePattern :: (String, String, String, [String])
    kvExtract txt = Map.fromList $ map (tuplify . map strip . splitOn "=" ) $ splitOn ";" txt
    tuplify l = case l of []    -> ("", "")
                          a:[]  -> (a, "")
                          a:b:_ -> (a, b)

parseRational :: Text -> Maybe Rational
parseRational t = do
  (x,_) <- listToMaybe $ readSigned readFloat (unpack t)
  return x

parseTuple2 :: Text -> Maybe (Rational, Rational)
parseTuple2 t = do
  let stripped = Data.Text.dropWhileEnd (== ')') $ Data.Text.dropWhile (== '(') t
  let nums = splitOn "," stripped
  num0s <- listToMaybe nums
  num0 <- parseRational num0s
  num1s <- (listToMaybe $ drop 1 nums)
  num1 <- parseRational num1s
  return (num0, num1)

-- | logging function wrapped in a Cluster monad
-- there's probably a better way to write this, but I'm new to Haskell
remoteComputeFailureHelper :: ProgramSpec -> Cluster SDPB.Output
remoteComputeFailureHelper spec = do
  Log.info "Program specification" spec
  Log.info "Params parsed from regex" params
  return SDPB.Output
    { terminateReason = SDPB.DualFeasibleJumpDetected
    , primalObjective = -1000000.0
    , dualObjective   = 0.0
    , dualityGap      = 1.0
    , primalError     = 50000.0
    , dualError       = 1.1
    , runtime         = 2*minute
    }
  where
    s = unpack $ rawCommand spec
    -- format: ONVec_delaunay_ngroup2_nmax6_a=b;c=d;e=f
    parsePattern = "^ONVec_([a-zA-Z0-9]*)_ngroup([0-9]*)_nmax([0-9]*)_(.*)$" :: String
    (_,_,_,params) = s =~ parsePattern :: (String, String, String, [String])
