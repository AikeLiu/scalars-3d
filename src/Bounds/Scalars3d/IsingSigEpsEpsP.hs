{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE OverloadedRecordDot    #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.IsingSigEpsEpsP where

import           Control.Monad.IO.Class     (liftIO)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Binary                (Binary)
import           Data.Data                  (Typeable)
import           Data.Matrix.Static         (Matrix)
import           Data.Reflection            (Reifies, reflect)
import           Data.Tagged                (Tagged (..), untag)
import           Data.Vector                (Vector)
import           GHC.Generics               (Generic)
import           GHC.TypeNats               (KnownNat)
import           Hyperion                   (Dict (..), Static (..), cPtr)
import           Hyperion.Bootstrap.Bound   (BuildInJob,
                                             SDPFetchBuildConfig (..),
                                             ToSDP (..), blockDir)
--import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
--                                                         SDPFetchBuildConfig (..),
--                                                         ToSDP (..), blockDir)
import           Blocks                     (Coordinate (ZZb), CrossingMat,
                                             DerivMultiplier (..), Derivative,
                                             TaylorCoeff (..), Taylors,
                                             crossOdd, zzbTaylors,
                                             zzbTaylorsAll)
import qualified Blocks                     as Blocks
import qualified Blocks.ScalarBlocks        as SB
import           Blocks.ScalarBlocks.Build  (scalarBlockBuildLink)
import           Bootstrap.Bounds           (BoundDirection, DeltaRange (..),
                                             FourPointFunctionTerm, HasRep (..),
                                             OPECoefficient,
                                             OPECoefficientExternal,
                                             Spectrum (..), boundDirSign,
                                             crossingMatrix,
                                             crossingMatrixExternal, derivsVec,
                                             listDeltas, mapBlocks, mapOps,
                                             opeCoeffExternalSimple,
                                             opeCoeffGeneric_,
                                             opeCoeffIdentical_, runTagged)
import qualified Bootstrap.Bounds           as Bounds
import           Bootstrap.Build            (FetchConfig (..),
                                             SomeBuildChain (..), noDeps)
import           Bootstrap.Math.FreeVect    (FreeVect, vec)
import           Bootstrap.Math.Linear      (bilinearPair, toV)
import qualified Bootstrap.Math.Linear      as L
import           Bootstrap.Math.VectorSpace (zero, (*^))
import qualified Bootstrap.Math.VectorSpace as VS
import           Linear.V                   (V)
import qualified SDPB
--import           SDPB.Blocks                            (Coordinate (ZZb),
--                                                         CrossingMat,
--                                                         DerivMultiplier (..),
--                                                         Derivative,
--                                                         TaylorCoeff (..),
--                                                         Taylors, crossOdd,
--                                                         zzbTaylors,
--                                                         zzbTaylorsAll)
--import qualified SDPB.Blocks                            as Blocks
--import qualified SDPB.Blocks.ScalarBlocks               as SB
--import           SDPB.Blocks.ScalarBlocks.Build         (scalarBlockBuildLink)
--import qualified SDPB.Bounds.BootstrapSDP               as Bounds
--import           SDPB.Bounds.BoundDirection             (BoundDirection,
--                                                         boundDirSign)
--import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
--                                                         HasRep (..),
--                                                         OPECoefficient,
--                                                         OPECoefficientExternal,
--                                                         crossingMatrix,
--                                                         crossingMatrixExternal,
--                                                         derivsVec, mapBlocks,
--                                                         opeCoeffExternalSimple,
--                                                         opeCoeffGeneric_,
--                                                         opeCoeffIdentical_,
--                                                         runTagged, mapOps)
--import           SDPB.Bounds.Spectrum                   (DeltaRange (..),
--                                                         Spectrum (..),
--                                                         listDeltas)
--import           SDPB.Build.BuildLink                   (SomeBuildChain (..),
--                                                         noDeps)
--import           SDPB.Build.Fetches                     (FetchConfig (..))
--import           SDPB.Math.FreeVect                     (FreeVect, vec)
--import           SDPB.Math.Linear.Literal               (toV)
--import qualified SDPB.Math.Linear.Util                  as L
--import           SDPB.Math.VectorSpace                  (bilinearPair, zero,
--                                                         (*^))
--import qualified SDPB.Math.VectorSpace                  as VS
--import qualified SDPB.SDP.Types                         as SDP


-- Ongoing Project


data ExternalDims = ExternalDims
  { deltaSigma :: Rational
  , deltaEps   :: Rational
  , deltaEpsP  :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data ExternalOp s = Sig | Eps | EpsP
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (SB.ScalarRep 3) where
  rep x@Sig  = SB.ScalarRep $ deltaSigma (reflect x)
  rep x@Eps  = SB.ScalarRep $ deltaEps (reflect x)
  rep x@EpsP = SB.ScalarRep $ deltaEpsP (reflect x)

data Z2Rep = Z2Even | Z2Odd
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, Binary, ToJSON, FromJSON)

data IsingSigEpsEpsP = IsingSigEpsEpsP
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum (Int, Z2Rep)
  , objective    :: Objective
  , spins        :: [Int]
  , blockParams  :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 6 Rational))
  | EpsilonOPEBound (V 6 Rational) BoundDirection
  | StressTensorOPEBound (Maybe (V 6 Rational)) BoundDirection
  | GFFNavigator (Maybe (V 6 Rational))
  -- | ShadowNavigator (Maybe (V 2 Rational))
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

crossingEquationsSSSS :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquationsSSSS g0 = toV
  ( (zzbTaylors crossOdd, gS Sig Sig Sig Sig)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)

crossingEquationsEEEP :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquationsEEEP g0 = toV
  ( (zzbTaylors crossOdd, gS Eps Eps Eps EpsP)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)

crossingEquationsPPPE :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquationsPPPE g0 = toV
  ( (zzbTaylors crossOdd, gS EpsP EpsP EpsP Eps)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)


crossingEquationsSSEE :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 2 (Taylors 'ZZb, FreeVect b a)
crossingEquationsSSEE g0 = toV
  ( (zzbTaylors crossOdd, gS Sig Eps Sig Eps)
  , (zzbTaylorsAll,       gS Sig Sig Eps Eps - gT Eps Sig Sig Eps)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)
    gT a b c d = g0 a b c d (SB.Standard4PtStruct, TChannel)

crossingEquationsEEPP :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 2 (Taylors 'ZZb, FreeVect b a)
crossingEquationsEEPP g0 = toV
  ( (zzbTaylors crossOdd, gS Eps EpsP Eps EpsP)
  , (zzbTaylorsAll,       gS Eps Eps EpsP EpsP - gT EpsP Eps Eps EpsP)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)
    gT a b c d = g0 a b c d (SB.Standard4PtStruct, TChannel)

crossingEquationsSSEP :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 2 (Taylors 'ZZb, FreeVect b a)
crossingEquationsSSEP g0 = toV
  ( (zzbTaylors crossOdd, gS Sig Eps Sig EpsP)
  , (zzbTaylorsAll,       gS Sig Sig Eps EpsP - gT Eps Sig Sig EpsP)
  )
  where
    gS a b c d = g0 a b c d (SB.Standard4PtStruct, SChannel)
    gT a b c d = g0 a b c d (SB.Standard4PtStruct, TChannel)


crossingEqsIsingSSEP :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 13 (Taylors 'ZZb, FreeVect b a)
crossingEqsIsingSSEP g =
  crossingEquationsSSSS g
  L.++ crossingEquationsSSSS (mapOps sigToeps g)
  L.++ crossingEquationsSSSS (mapOps sigToepsP g)
  L.++ crossingEquationsSSEE g
  L.++ crossingEquationsSSEE (mapOps epsToepsP g)
  L.++ crossingEquationsEEPP g
  --L.++ crossingEquationsSSEE (mapOps sigToepsP g)
  L.++ crossingEquationsEEEP g
  L.++ crossingEquationsPPPE g
  L.++ crossingEquationsSSEP g
  where
    sigToeps  :: ExternalOp s -> ExternalOp s
    sigToeps  Sig = Eps
    sigToeps  op  = op
    epsToepsP :: ExternalOp s -> ExternalOp s
    epsToepsP Eps = EpsP
    epsToepsP op  = op
    sigToepsP :: ExternalOp s -> ExternalOp s
    sigToepsP Sig = EpsP
    sigToepsP op  = op


isingDerivsVec :: IsingSigEpsEpsP -> V 13 (Vector (TaylorCoeff (Derivative 'ZZb)))
isingDerivsVec i = fmap ($ nmax) (derivsVec crossingEqsIsingSSEP)
  where
    nmax = (blockParams i).nmax

isingSEEPCrossingMat
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3) a)
  -> Tagged s (CrossingMat j 13 (SB.ScalarBlock 3) a)
isingSEEPCrossingMat channel =
  pure $ mapBlocks SB.ScalarBlock $
  crossingMatrix channel (crossingEqsIsingSSEP @s)

data Channel j where
  Z2_Even_Spin_Even :: SB.SymTensorRep 3 -> Channel 4
  Z2_Even_Spin_Odd :: SB.SymTensorRep 3 -> Channel 1
  Z2_Odd  :: SB.SymTensorRep 3 -> Channel 2
  IdentityChannel     :: Channel 1
  StressTensorChannel :: Channel 1
  ExternalOpChannel   :: Channel 6

mat
  :: forall s a j . (Reifies s ExternalDims, Fractional a, Eq a)
  => Channel j
  -> Tagged s (CrossingMat j 13 (SB.ScalarBlock 3) a)
mat (Z2_Even_Spin_Even internalRep) = isingSEEPCrossingMat $ toV
  ( opeCoeffIdentical_ Sig internalRep (vec ())
  , opeCoeffIdentical_ Eps internalRep (vec ())
  , opeCoeffIdentical_ EpsP internalRep (vec ())
  , opeCoeffGeneric_ Eps EpsP internalRep (vec ()) ((-1)^l *^ vec ())
  )
  where
    l = SB.symTensorSpin internalRep

mat (Z2_Even_Spin_Odd internalRep) = isingSEEPCrossingMat $ toV
  (
   opeCoeffGeneric_ Eps EpsP internalRep (vec ()) ((-1)^l *^ vec ())
  )
  where
    l = SB.symTensorSpin internalRep

mat (Z2_Odd internalRep) = isingSEEPCrossingMat $ toV $
  ( opeCoeffGeneric_ Sig Eps internalRep (vec ()) ((-1)^l *^ vec ())
  , opeCoeffGeneric_ Sig EpsP internalRep (vec ()) ((-1)^l *^ vec ())
--  , opeCoeffGeneric_ Eps EpsP internalRep (vec ()) ((-1)^l *^ vec ())
  )
  where
    l = SB.symTensorSpin internalRep

mat IdentityChannel = isingSEEPCrossingMat (toV identityOpe)
  where
    identityRep = SB.SymTensorRep (Blocks.Fixed 0) 0
    identityOpe o1 o2
      | o1 == o2  = vec (SB.Standard3PtStruct (rep o1) (rep o2) identityRep)
      | otherwise = 0

mat StressTensorChannel = isingSEEPCrossingMat (toV stressTensorOpe)
  where
    stressTensorRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 2
    stressTensorOpe o1 o2
      | o1 == o2  =
        fromRational (SB.scalarDelta (rep o1)) *^
        vec (SB.Standard3PtStruct (rep o1) (rep o2) stressTensorRep)
      | otherwise = 0

mat ExternalOpChannel = pure . mapBlocks SB.ScalarBlock $
  crossingMatrixExternal opeCoefficients crossingEqsIsingSSEP [Sig, Eps, EpsP]
  where
    opeCoefficients :: V 6 (OPECoefficientExternal (ExternalOp s) (SB.Standard3PtStruct 3) a)
    opeCoefficients = toV ( opeCoeffExternalSimple Sig Sig Eps (vec ())
                          , opeCoeffExternalSimple Sig Sig EpsP (vec ())
                          , opeCoeffExternalSimple Eps Eps Eps (vec ())
                          , opeCoeffExternalSimple EpsP EpsP EpsP (vec ())
                          , opeCoeffExternalSimple EpsP EpsP Eps (vec ())
                          , opeCoeffExternalSimple EpsP Eps Eps (vec ())
                          )

getExternalMat
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => IsingSigEpsEpsP
  -> f (Matrix 6 6 (Vector a))
getExternalMat i =
  Bounds.getIsolatedMat (blockParams i) (isingDerivsVec i) (runTagged (externalDims i) (mat ExternalOpChannel))

bulkConstraints
  :: forall a s m.
     ( RealFloat a, Binary a, Typeable a
     , Reifies s ExternalDims
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     , Applicative m
     )
  => IsingSigEpsEpsP
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints i@IsingSigEpsEpsP{..} = pure $ do
  z2Rep <- [minBound .. maxBound]
  l <- case z2Rep of
    Z2Odd  -> spins
    Z2Even -> spins
  (delta, range) <- listDeltas (l, z2Rep) spectrum
  let intRep = SB.SymTensorRep delta l
  pure $ case (z2Rep, even l) of
    (Z2Even,True) -> Bounds.bootstrapConstraint blockParams dv range $ untag @s $ mat $ Z2_Even_Spin_Even intRep
    (Z2Even,False)-> Bounds.bootstrapConstraint blockParams dv range $ untag @s $ mat $ Z2_Even_Spin_Odd intRep
    (Z2Odd, _)    -> Bounds.bootstrapConstraint blockParams dv range $ untag @s $ mat $ Z2_Odd  intRep
  where
    dv = isingDerivsVec i

isingSigEpsEpsPSDP
  :: forall a m. ( RealFloat a, Applicative m, Binary a, Typeable a
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => IsingSigEpsEpsP
  -> SDPB.SDP m a
isingSigEpsEpsPSDP i@IsingSigEpsEpsP{..} = runTagged externalDims $ do
    epsMat <- mat ExternalOpChannel
    bulk   <- bulkConstraints i
    unit   <- mat IdentityChannel
    stress <- mat StressTensorChannel
    let
      dv = isingDerivsVec i
      epsCons mLambda = case mLambda of
        Nothing     -> Bounds.bootstrapConstraint blockParams dv Isolated epsMat
        Just lambda -> Bounds.bootstrapConstraint blockParams dv Isolated $
                       bilinearPair (fmap fromRational lambda) epsMat
      stressCons = Bounds.bootstrapConstraint blockParams dv Isolated stress
    (cons, obj, norm) <- case objective of
      Feasibility mLambda -> pure
        ( bulk ++ [ epsCons mLambda, stressCons]
        , zero
        , unit
        )
      StressTensorOPEBound mLambda dir -> pure
        ( bulk ++ [epsCons mLambda]
        , unit
        , boundDirSign dir *^ stress
        )
      EpsilonOPEBound lambda dir -> pure
        ( bulk ++ [stressCons]
        , unit
        , boundDirSign dir *^ bilinearPair (fmap fromRational lambda) epsMat
        )
      GFFNavigator mLambda -> do
        let ExternalDims { deltaSigma, deltaEps, deltaEpsP } = externalDims
        -- (2*deltaSigma + 2 ~ 3.036)
        -- (2*deltaEps + 2  ~ 4.8)
        -- (2*deltaEpsP ~ 7.6)
        -- (deltaEps + deltaEpsP ~ 5.2 )
        -- (deltaSigma + deltaEps + 2 ~ 3.918)
        -- (deltaSigma + deltaEpsP ~ 4.318)
        -- (deltaSigma + deltaEpsP + 2 ~ 6.518)
            identicalN1 delta = delta * (sqrt ((2 * (2*delta - 1))/(3 * (4*delta - 1))))
            identicalN2 delta = (delta + 2 * delta * delta -1) * (sqrt ((delta * (2*delta + 1))/(30 * (4*delta+1) * (4*delta+3))))
            diffN1 delta1 delta2 = sqrt (( delta1 * delta2 * (2*delta1 - 1) * (2*delta2 - 1)) / (3 * (delta1 + delta2 - 1) * (2 * delta1 + 2 * delta2 - 1)) )
            identicalS2 delta = delta * (sqrt ((2 * (delta + 1))/((2*delta + 1))))
        sigSq  <- bilinearPair (toV (sqrt 2, 0, 0,0 )) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaSigma)) 0))
        sigN1Sq  <- bilinearPair (toV ((identicalN1 $ fromRational deltaSigma),  0, 0, 0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaSigma+2)) 0))
        sigN2Sq  <- bilinearPair (toV ((identicalN2 $ fromRational deltaSigma), 0, 0, 0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaSigma+4)) 0))
        epsSq  <- bilinearPair (toV (0, sqrt 2, 0, 0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaEps)) 0))
        epsN1Sq  <- bilinearPair (toV (0, (identicalN1 $ fromRational deltaEps),0, 0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaEps+2)) 0))
--        epsPSq  <- bilinearPair (toV (0, 0, sqrt 2, 0)) <$>
--                  mat (Z2_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaEpsP)) 0))
        sigEps <- bilinearPair (toV (1, 0)) <$>
                  mat (Z2_Odd  (SB.SymTensorRep (Blocks.Fixed (deltaEps+deltaSigma)) 0))
        sigEpsN1 <- bilinearPair (toV ((diffN1 (fromRational deltaSigma) (fromRational deltaEps)),0)) <$>
                  mat (Z2_Odd  (SB.SymTensorRep (Blocks.Fixed (deltaEps+deltaSigma+2)) 0))
        sigEpsP <- bilinearPair (toV (0, 1)) <$>
                  mat (Z2_Odd  (SB.SymTensorRep (Blocks.Fixed (deltaEpsP+deltaSigma)) 0))
        epsEpsP <- bilinearPair (toV (0,0,0,1)) <$>
                  mat (Z2_Even_Spin_Even  (SB.SymTensorRep (Blocks.Fixed (deltaEps+deltaEpsP)) 0))
        sigSpin2 <- bilinearPair (toV ((identicalS2 $ fromRational deltaSigma), 0, 0, 0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaSigma+2)) 2))
        epsSpin2 <- bilinearPair (toV (0, (identicalS2 $ fromRational deltaEps), 0,0)) <$>
                  mat (Z2_Even_Spin_Even (SB.SymTensorRep (Blocks.Fixed (2*deltaEps+2)) 2))
        let gff = VS.sum [sigSq, epsSq,sigN1Sq,sigN2Sq, epsN1Sq, sigEps, sigEpsP, sigSpin2, epsSpin2]
        pure
          ( bulk ++ [epsCons mLambda, stressCons]
          , unit
          , (-1) *^ gff
          )
      --ShadowNavigator mLambda -> do
      --  let ExternalDims { deltaSigma } = externalDims
      --  shadow <- bilinearPair (toV 1) <$>
      --            mat (Z2_Odd (SB.SymTensorRep (Blocks.Fixed (3-deltaSigma)) 0))
      --  pure
      --    ( bulk ++ [epsCons mLambda, stressCons]
      --    , unit
      --    , (-1) *^ shadow
      --    )
    return $ SDPB.SDP
      { SDPB.objective = Bounds.bootstrapObjective blockParams dv obj
      , SDPB.normalization= Bounds.bootstrapNormalization blockParams dv norm
      , SDPB.positiveConstraints= cons
      }

instance ToSDP IsingSigEpsEpsP where
  type SDPFetchKeys IsingSigEpsEpsP = '[ SB.BlockTableKey ]
  toSDP = isingSigEpsEpsPSDP

instance SDPFetchBuildConfig IsingSigEpsEpsP where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . SB.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig cftBoundFiles False

-- TODO: Template Haskell?
instance Static (Binary IsingSigEpsEpsP)              where closureDict = cPtr (static Dict)
instance Static (Show IsingSigEpsEpsP)                where closureDict = cPtr (static Dict)
instance Static (ToSDP IsingSigEpsEpsP)               where closureDict = cPtr (static Dict)
instance Static (ToJSON IsingSigEpsEpsP)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig IsingSigEpsEpsP) where closureDict = cPtr (static Dict)
instance Static (BuildInJob IsingSigEpsEpsP)          where closureDict = cPtr (static Dict)

