#!/bin/bash

module unload PrgEnv-intel
module load PrgEnv-gnu
module load eigen3/3.3.7-gnu
module load openmpi

echo /global/homes/w/wlandry/gnu_openmpi/install/bin/scalar_blocks $@
/global/homes/w/wlandry/gnu_openmpi/install/bin/scalar_blocks $@
