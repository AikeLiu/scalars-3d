-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion


scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "pi-symmetry-aike" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/gpfs/aliu/hyperion-test")
  { emailAddr = Nothing}
